<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contato extends Model
{
    use SoftDeletes;

    protected $fillable = ['nome'];

    public function celulares()
    {
        return $this->hasMany(ContatoInfo::class, 'id_contato')->where('tipo', 'celular');
    }

    public function emails()
    {
        return $this->hasMany(ContatoInfo::class, 'id_contato')->where('tipo', 'email');
    }

    public function telefones()
    {
        return $this->hasMany(ContatoInfo::class, 'id_contato')->where('tipo', 'telefone');
    }
}
