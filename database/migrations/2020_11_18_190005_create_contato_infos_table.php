<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatoInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato_infos', function (Blueprint $table) {
            $table->id();
            $table->enum('tipo', ['celular', 'email', 'telefone']);
            $table->string('valor');
            $table->unsignedBigInteger('id_contato');
            $table->foreign('id_contato')->references('id')->on('contatos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contato_infos');
    }
}
