<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContatosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $telefonesCount = count($this->telefones);
        $emailsCount = count($this->emails);
        $celularesCount = count($this->celulares);

        return [
            'id'              => $this->id,
            'nome'            => $this->nome,
            'telefones_count' => $telefonesCount ?? 0,
            'emails_count'    => $emailsCount,
            'celulares_count' => $celularesCount ?? 0,
            'infos' => [
                'telefones'       => $telefonesCount > 0 ? $this->telefones->pluck('valor') : [],
                'emails'          => $emailsCount > 0 ? $this->emails->pluck('valor') : [],
                'celulares'       => $celularesCount > 0 ? $this->celulares->pluck('valor') : []
            ]            
        ];
    }
}
