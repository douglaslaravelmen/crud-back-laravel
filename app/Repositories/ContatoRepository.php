<?php

namespace App\Repositories;

use App\Models\Contato;
use App\Models\ContatoInfo;

class ContatoRepository
{

    public static function getAll()
    {
        return Contato::with('celulares', 'emails', 'telefones')->get();
    }

    public static function getById($id)
    {
        return Contato::with('celulares', 'emails', 'telefones')->findOrFail($id);
    }

    public static function storeOrUpdate($dados, $id = null)
    {   
        $contato = Contato::updateOrCreate(
            [
                'id' => $id
            ],
            [
                'nome' => $dados->nome
            ]
        );

        self::saveInfos($dados, $contato);

        return $contato->load('celulares', 'emails', 'telefones');
    }

    public static function deleteById($id)
    {
        Contato::findOrFail($id)->delete();
    }

    public static function saveInfos($dados, $contato)
    {
        $contato->celulares()->delete();
        $contato->emails()->delete();
        $contato->telefones()->delete();
        
        if (isset($dados->infos['celulares'])) {
            foreach ($dados->infos['celulares'] as $celular) {
                ContatoInfo::create([
                    'tipo' => 'celular',
                    'valor' => $celular,
                    'id_contato' => $contato->id
                ]);
            }
        }

        if (isset($dados->infos['emails'])) {
            foreach ($dados->infos['emails'] as $email) {
                ContatoInfo::create([
                    'tipo' => 'email',
                    'valor' => $email,
                    'id_contato' => $contato->id
                ]);
            }
        }

        if (isset($dados->infos['telefones'])) {
            foreach ($dados->infos['telefones'] as $telefone) {
                ContatoInfo::create([
                    'tipo' => 'telefone',
                    'valor' => $telefone,
                    'id_contato' => $contato->id
                ]);
            }
        }
    }
}
