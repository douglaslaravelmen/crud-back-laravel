<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContatoInfo extends Model
{
    protected $fillable = ['tipo', 'valor', 'id_contato'];

    public function contato()
    {
        return $this->belongsTo(Contato::class, 'id_contato');
    }
}
